#Functie om invoer op te vragen
def AskForNumberSequence(Message):
    Invoer = input(Message)

    #Invoer splitsen en van string naar int omzetten + return
    return ([int(byte) for byte in Invoer.split(".")])

#Geldigheid van het ip adres controleren
def IsValidIpAddress(NumberList):
    for Byte in NumberList:
        #Grootte van elke byte controleren
        if (Byte >= 0 and Byte <= 255):
            return True
        #Lengte van het adres controleren
        if len(NumberList) == 4:
            return True
        else:
            return False
    
#Geldigheid van het subnetmasker controleren
def IsValidNetmask(NumberList):
    BinaryNetmask = ""
    CheckingOnes = True
    #Lengte van het subnetmasker
    for Byte in NumberList:
        if len(NumberList) != 4:
            return False
        else:
           #Als de lengte klopt, het subnetmasker in binaire vorm omzetten
           BinaryByte = (f"{Byte:08b}")
           BinaryNetmask += BinaryByte
        #Controleren of waarden van subnet masker kloppen
        if (Byte >= 0 and Byte <= 255):
            return True
        else:
            return False

    #Bits in het masker controleren of ze op 1 of 0 staan
    for bytBytee in BinaryNetmask:
        if CheckingOnes and Byte == 0:
            CheckingOnes = False
        elif CheckingOnes == False and Byte == 1:
            return False
    return True

def OneBitsInNetmask(NetmaskList):
    #Variabelen aanmaken
    Counter = 0
    NetmaskerString = ""
    BinaireMaskLijst = []

    #Subnetmasker lijst omzetten in binaire notatie
    for Byte in NetmaskList:
        BinaireNetMaskByte = int("{0:08b}".format(Byte))
        BinaireMaskLijst.append(BinaireNetMaskByte)

    #Lijst omzetten naar string
    NetmaskerString =  ".".join(map(str, BinaireMaskLijst))

    #Het aantal one-bits in het masker tellen
    for Bit in NetmaskerString:
        if Bit == "1":
           Counter += 1
    return Counter

def ApplyNetworkMask(HostAddress, Netmask):
    #Aanmaken variabelen voor op slaan binaire voorstellingen
    NetworkMask = []
    #Network masker berekenen
    for HostByte, MaskByte in zip(HostAddress, Netmask):
        NetWorkByte = (HostByte & MaskByte)
        NetworkMask.append(NetWorkByte)
    return NetworkMask
    
def NetmaskToWildcardmask(Netmasker):
    #Variabelen declareren
    i = 0
    ByteLijst = [0,0,0,0]
    BinaryByte = ""

    #1'en in 0'n omzetten en omgekeerd
    for byte in Netmasker:
        #Elke byte van invoer omzetten naar binaire string
        BinaryByte = (f"{byte:08b}")
        NotByte = ""
        #Elk karakter in binaire string bitwise NOT op toepassen
        for Bit in BinaryByte:
            if Bit == "1":
                NotByte += "0"
            elif Bit == "0":
                NotByte += "1"
        ByteLijst[i] = int(NotByte, 2)
        i += 1
    return ByteLijst

def GetBroadcastAddress(NetworkAddress, WildcardMask):
    #Variabelen declareren
    i = 0
    BroadcastAdress = []

    #Bitwise OR nemen van netwerk en wildcardmasker
    for Byte in NetworkAddress:
        BroadcastAdress.append(Byte | WildcardMask[i])
        i += 1
    return BroadcastAdress

def PrefixLengthToMaxHosts(SubnetLengte):
    #Met formule aantal hosts berekenen
    HostLengte = (32 - SubnetLengte )
    AantalHosts = (2**HostLengte) - 2
    return AantalHosts

#Uitvoeren programma
if __name__ == '__main__':

    #Waarden ophalen en in variabelen plaatsen
    HostAddress = AskForNumberSequence("Wat is het IP-adres?\n")
    SubnetMask = AskForNumberSequence("Wat is het subnetmasker?\n")
    
    #Controleren of invoer klopt
    if IsValidIpAddress(HostAddress) and IsValidNetmask(SubnetMask):

        #Aantal hostbits uit functie halen
        AantalHostBits = OneBitsInNetmask(SubnetMask)

        #Network mask uit functie halen en omzetten in string
        networkMask = ApplyNetworkMask(HostAddress,SubnetMask)
        networkMaskString = ".".join(map(str, networkMask))

        #Wildcard mask uit functie halen en omzetten in string
        WildcardMask = NetmaskToWildcardmask(SubnetMask)
        WilcardMaskString = ".".join(map(str, WildcardMask))

        #Boradcast adres uit functie halen en omzetten in string
        BroadcastAdress = GetBroadcastAddress(networkMask, WildcardMask)
        BroadCastString = ".".join(map(str, BroadcastAdress))

        #Maximaal aantal hosts uit functie halen
        MaximumAantalHosts = PrefixLengthToMaxHosts(AantalHostBits)

        #Uitprinten van uitvoer functies
        print("IP-adres en subnetmasker zijn geldig.")
        print("De lengte van het subnetmasker is {}".format(AantalHostBits))
        print("Het adres van het subnet is {}".format(networkMaskString))
        print("Het wildcard masker is {}".format(WilcardMaskString))
        print("Het broadcastadres is {}".format(BroadCastString))
        print("Het maximaal aantal hosts op dit subnet is {}".format(MaximumAantalHosts))
    else:
        print("De invoer is niet geldig, voer het programma opnieuw uit.")